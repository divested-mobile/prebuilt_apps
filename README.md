Prebuilt Apps Repo
------------------

This repo contains a script and a makefile for managing prebuilt apps, specifically ones from F-Droid.

You will need to import the necessary signing keys in order to verify them
source update.sh && importKeys

A shim to ease installation of Fennec DOS is also included, it contains no code.
